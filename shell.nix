let
  pkgs = import <nixpkgs> { };
  stdenv = pkgs.stdenv;
in rec {
  armsEnv = stdenv.mkDerivation {
    name = "dev";
    buildInputs = [
      stdenv
      pkgs.mosquitto
    ];
    MQTTLIB="${pkgs.mosquitto}/lib";
    MQTTINC="${pkgs.mosquitto}/include";
  };
}
