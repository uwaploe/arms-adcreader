
CC = gcc
CPPFLAGS = -I$(MQTTINC)
CFLAGS = -Wall -O2

PROG = adcreader

all: $(PROG)

adcreader.o: adcreader.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<

adcreader: adcreader.o
	$(CC) -o $@ $(CFLAGS) -L$(MQTTLIB) $^ -lrt -lmosquitto

clean:
	rm -f adcreader *.o *~