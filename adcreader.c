/*
 * Read the internal A/D on a Versalogic Iguana SBC and publish the
 * data records to a Mosquitto MQTT broker.
 *
 */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <mosquitto.h>


#define ADC_CHANS       8
#define MQTT_CLIENT     "adc-pub"
#define MQTT_HOST       "localhost"
#define MQTT_PORT       1883
#define MQ_NAME         "/adc"
#define MQ_MODE         0666
#define MAX_DEGREE      6


struct Msg {
    struct timespec   ts;
    int               data[ADC_CHANS];
};

struct Calibration {
    char            *name;
    char            *units;
    int             degree;
    double          C[MAX_DEGREE];
};

struct State {
    mqd_t               mq;
    struct mosquitto    *m;
    bool                ready;
    struct Calibration  *cal[ADC_CHANS];
};

static int intr_signals[] = {SIGHUP, SIGINT, SIGQUIT, SIGTERM, SIGALRM};
volatile sig_atomic_t interrupted = 0;

#define NR_INTS (sizeof(intr_signals)/sizeof(int))


static void
catch_signal(int signum)
{
    if(signum != SIGALRM)
        interrupted = 1;
}

/*
 * Thread to listen for A/D data records on a POSIX message queue,
 * reformat them and publish as messages to the MQTT Broker.
 */
static void*
publisher(void *thread_arg)
{
    struct State        *st = (struct State*)thread_arg;
    int                 i, j;
    double              volts, y;
    struct Msg          msg;
    sigset_t            mask;
    int64_t             ts;
    struct Calibration  *cp;
    char                record[256];

    pthread_detach(pthread_self());

    /*
     * Insure that any interrupting signals are delivered to the main thread.
     */
    sigemptyset(&mask);
    for(i = 0;i < NR_INTS;i++)
        sigaddset(&mask, intr_signals[i]);

    pthread_sigmask(SIG_BLOCK, &mask, NULL);

    fprintf(stderr, "Waiting for messages ...\n");
    while(1)
    {
        if(mq_receive(st->mq, (char*)&msg, sizeof(msg), NULL) < 0)
        {
            error(0, errno, "mq_receive failed\n");
            break;
        }

        if(st->ready)
        {
            /* Timestamp in microseconds since the epoch */
            ts = (int64_t)msg.ts.tv_sec * 1000000 + (int64_t)msg.ts.tv_nsec/1000;

            for(i = 0;i < ADC_CHANS;i++)
            {
                /* Convert A/D counts to volts */
                volts = msg.data[i] * 5. / 4096.;
                if((cp = st->cal[i]) != NULL)
                {
                    /* Apply calibration polynomial */
                    y = 0.;
                    for(j = cp->degree-1;j >= 0;j--)
                        y = y * volts + cp->C[j];
                    /*
                     * Format messages using the Line Protocol.
                     *
                     * https://docs.influxdata.com/influxdb/v1.1/write_protocols/line_protocol_tutorial/
                     */
                    snprintf(record, sizeof(record),
                             "%s value=%.3f,units=\"%s\" %ld",
                             cp->name, y, cp->units, ts);
                    mosquitto_publish(st->m, NULL,
                              "sensors/eng",
                              (int)strlen(record),
                              record,
                              0,
                              false);
                }

                /* Also publish the raw voltages */
                snprintf(record, sizeof(record),
                         "ai%d voltage=%6.4f %ld",
                         i, volts, ts);
                mosquitto_publish(st->m, NULL,
                                  "sensors/engraw",
                                  (int)strlen(record),
                                  record,
                                  0,
                                  false);
            }

        }
    }

    return NULL;
}

/*
 * Connect event handler for MQTT client.
 */
static void
on_connect(struct mosquitto *m, void *udata, int res)
{
    struct State    *sp = (struct State*)udata;

    if(res == 0)
    {
        sp->m = m;
        sp->ready = true;
    }
    else
        error(1, errno, "Cannot connect to MQTT Broker\n");
}

/*
 * Read an A/D record.
 */
static bool
read_adc(struct Msg *mp)
{
    FILE    *fp;
    bool    status = true;
    int     i;
    char    dot;

    if((fp = fopen("/proc/iguana/adc", "rb")) != NULL)
    {
        fscanf(fp, "%ld%1c%ld", &mp->ts.tv_sec, &dot, &mp->ts.tv_nsec);
        mp->ts.tv_nsec *= 1000;
        for(i = 0;i < ADC_CHANS;i++)
            fscanf(fp, "%d", &mp->data[i]);

        fclose(fp);
    }
    else
        status = false;

    return status;
}

/*
 * Load the A/D calibration data from an ASCII file with lines of
 * the form:
 *
 *   CHANNEL,NAME,UNITS,C0 C1 [C2 ... CN]
 *
 * CHANNEL    = A/D channel number
 * NAME       = data variable name
 * UNITS      = data units
 * C0 ... CN  = coefficients of calibration polynomial
 *
 */
static void
load_cal(const char *filename, struct State *sp)
{
    FILE                *fp;
    int                 i, idx = -1, count = -1, linenum;
    struct Calibration  *cp;
    size_t              nbytes = 100;
    char                *line, *p;

    fp = fopen(filename, "rb");
    if(fp == NULL)
        error(1, errno, "Cannot open calibration file\n");

    line = (char*)malloc(nbytes + 1);
    if(!line)
        error(1, errno, "Out of memory\n");

    linenum = 1;
    while(getline(&line, &nbytes, fp) > 0)
    {
        if(line[0] == '#' || nbytes < 2)
            continue;

        cp = (struct Calibration*)malloc(sizeof(struct Calibration));
        if(!cp)
            error(1, errno, "Out of memory\n");

        /* Read CHANNEL,NAME,UNITS, */
        if(sscanf(line, "%d,%m[^,],%m[^,],%n",
                  &idx, &cp->name, &cp->units, &count) != 4)
            error_at_line(1, 0, filename, linenum, "Bad calibration format\n");

        if(idx < 0 || idx >= ADC_CHANS)
            error_at_line(1, 0, filename, linenum, "Bad channel index\n");

        /* Read space-separated polynomial coefficients */
        i = 0;
        p = strtok(&line[count], " \t\r\n");
        while(p)
        {
            cp->C[i++] = atof(p);
            p = strtok(NULL, " \t\r\n");
        }
        cp->degree = i;
        sp->cal[idx] = cp;
        linenum++;
    }
}


int
main(int ac, char **av)
{
    int                 err, i;
    struct State        state;
    struct mosquitto    *m;
    struct Msg          msg;
    struct mq_attr      attr;
    pthread_t           tid;
    struct sigaction    sa, old_sa[NR_INTS];
    struct itimerval    timer;

    memset(&state, 0, sizeof(state));

    if(ac > 1)
        load_cal(av[1], &state);

    /* Install signal handler */
    sa.sa_handler = catch_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    for(i = 0;i < NR_INTS;i++)
        sigaction(intr_signals[i], &sa, &old_sa[i]);

    /* Message queue attributes */
    attr.mq_maxmsg = 8;
    attr.mq_msgsize = sizeof(struct Msg);
    attr.mq_flags = 0;

    state.mq = mq_open(MQ_NAME, O_CREAT|O_RDWR, MQ_MODE, &attr);
    if(state.mq < 0)
        error(1, errno, "Cannot open message queue\n");

    /* Initialize MQTT Client */
    mosquitto_lib_init();
    m = mosquitto_new(MQTT_CLIENT, true, (void*)&state);
    if(m == NULL)
        error(1, errno, "Out of memory\n");
    mosquitto_connect_callback_set(m, on_connect);
    err = mosquitto_connect(m, MQTT_HOST, MQTT_PORT, 60);
    if(err != MOSQ_ERR_SUCCESS)
        error(err, errno, "Cannot connect to MQTT broker\n");

    /* Start client thread */
    mosquitto_loop_start(m);

    /* Start publisher */
    err = pthread_create(&tid, NULL, publisher, &state);
    if(err != 0)
        error(err, errno, "Cannot create thread\n");

    /* Set interval timer */
    timer.it_interval.tv_sec = 2;
    timer.it_interval.tv_usec = 0;
    timer.it_value.tv_sec = 2;
    timer.it_value.tv_usec = 0;
    setitimer(ITIMER_REAL, &timer, NULL);

    while(!interrupted)
    {
        /* Wait for signal */
        pause();

        if(read_adc(&msg))
        {
            if(mq_send(state.mq, (const char*)&msg, sizeof(msg), 1) < 0)
            {
                error(0, errno, "Message send failed\n");
                break;
            }
        }
        else
        {
            error(0, errno, "Cannot read ADC data\n");
            break;
        }
    }

    /* Clean up ... */
    mosquitto_disconnect(m);
    mosquitto_loop_stop(m, true);
    mosquitto_destroy(m);
    mosquitto_lib_cleanup();

    mq_close(state.mq);
    mq_unlink(MQ_NAME);

    for(i = 0;i < ADC_CHANS;i++)
        if(state.cal[i] != NULL)
        {
            free(state.cal[i]->name);
            free(state.cal[i]->units);
            free(state.cal[i]);
        }

    return 0;
}
