# Iguana A/D Reader

This is
a [Systemd-based](https://www.freedesktop.org/wiki/Software/systemd/)
service to read the A/D on a Versalogic Iguana SBC on a periodic basis and
publish the data to an [MQTT Broker](http://mqtt.org).

## Requirements

 * A Versalogic Iguana SBC with the [Iguana ADC driver]() installed.
 * [Mosquitto](https://mosquitto.org) MQTT Broker.

## Installation

``` shellsession
$ make
$ sudo make install
$ sudo systemctl enable adcreader
$ sudo systemctl start adcreader
```
